import "./Button.scss";

const Button = ({ buttonName, color }) => {
  return (
    <button className="button" style={{ backgroundColor: color }}>
      {buttonName}
    </button>
  );
};

export default Button;
