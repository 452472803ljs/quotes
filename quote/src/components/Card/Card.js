import "./Card.scss";
import Button from "../Button/Button";
const Card = () => {
  return (
    <div className="Container">
      <div className="Quote">
        <span style={{ fontSize: "80px" }}>"</span>
        <p>
          Sentense
          SentenseSentenseSentenseSentenseSentenseSentenseSentenseSentense
          Sentense Sentense Sentense Sentense Sentense Sentense Sentense
          Sentense Sentense
        </p>
      </div>
      <p>author</p>
      <div className="buttons">
        <Button buttonName="Twitter" color="blue" />
        <Button buttonName="New Quote" />
      </div>
    </div>
  );
};

export default Card;
